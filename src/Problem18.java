import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        int number = sc.nextInt();
        
        if(number == 1){
            System.out.print("Please input number: ");
            int number1 = sc.nextInt();
            for(int i = 0 ; i < number1 ; i++ ){
                for(int j = 0 ; j < number1 ; j++ ){
                    if(j<=i){
                        System.out.print("*");
                    }
                }
                System.out.println();
            }
        }
        else if(number == 2){
            System.out.print("Please input number:");
            int number2 = sc.nextInt();
            for(int i = 0 ; i < number2 ; i++ ){
                for(int j = 0 ; j < number2 ; j++ ){
                    if(j>=i){
                        System.out.print("*");
                    }
                }
                System.out.println();
            }
        }
        else if(number == 3){
            System.out.print("Please input number:");
            int number3 = sc.nextInt();
            for(int i = 0 ; i < number3 ; i++ ){
                for(int j = 0 ; j < number3 ; j++ ){
                    if(i > j){
                        System.out.print("");
                    }
                    else {
                        System.out.println("*");
                    }
                }
                System.out.println();
            }
        }
        else if(number == 4){
            System.out.print("Please input number:");
            int number4 = sc.nextInt();
            for(int i = 0 ; i < number4 ; i++ ){
                for(int j = 0 ; j < number4 ; j++ ){
                    if((i+j)>3){
                        System.out.print("*");
                    }
                    else {
                        System.out.println(" ");
                    }
                }
                System.out.println();
            }
        }
        else if(number == 5){
            System.out.print("Bye bye!!!");
            
        }
        else {
            System.out.print("Error: Please input number between1-5");
        }
    }
    
}
