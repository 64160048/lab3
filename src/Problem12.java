public class Problem12 {
    public static void main(String[] args) {
        /*
            0 01234 *****
            1 01234  ****
            2 01234   ***
            3 01234    **
            4 01234     *

        */
        
        for(int i = 0;i<5;i++){
            for(int j=0; j<5;j++){ 
                if (j>=i){
                    System.out.print("*");
                }else {
                    System.out.print(" ");
                }
                
            }
            System.out.println();       
        }

    }
    
}
