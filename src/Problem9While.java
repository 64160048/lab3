import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int Num ;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input: ");
        Num = sc.nextInt();
    
        int i=1;
        while( i<=Num ) {
            int j=1;
            while( j<=Num ) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
       }
        
    }
    
}
